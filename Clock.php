<?php

class Clock
{
    public function __construct(int $hour = 0, int $minute = 0) {

        $this->hour = $hour;
        $this->minute = $minute;
        $this->date = new DateTime("now");
        $this->setTime();
    }
    
    public function __toString(): string
    {
        return date_format($this->date, 'H:i');
    }

    public function add($min)
    {
        $this->minute += $min;
        $this->setTime();
        return $this;
    }

    public function sub($min)
    {
        $this->minute -= $min;
        $this->setTime();
        return $this;
    }

    public function setTime()
    {
        while($this->minute<0) {
            $this->hour -= 1;
            $this->minute += 60;
        }

        if ($this->minute >= 60) {
            $this->hour += ($this->minute / 60) % 24;
        }

        if ($this->hour < 0) {
            $this->hour = 24 - (-$this->hour % 24) ;
        }

        if ($this->hour >= 24) {
            $this->hour = $this->hour % 24;
        }


        $this->minute += ($this->hour / 24) % 60;
        $this->minute = $this->minute % 60;
        date_time_set($this->date, $this->hour, $this->minute);
    }
}
