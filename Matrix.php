<?php

class Matrix
{
    public function __construct($matrix)
    {
        $this->matrixArray = explode("\n", $matrix);
        $this->transformMatrix();
    }

    public function getRow($rowIndex)
    {
        return $this->matrixArray[$rowIndex - 1];
    }

    public function getColumn($colIndex)
    {
        $tempArray = [];

        foreach ($this->matrixArray as $row) {
            array_push($tempArray, $row[$colIndex-1]);
        }

        return $tempArray;
    }

    public function transformMatrix(): void
    {
        foreach ($this->matrixArray as $index => $value) {
            $this->matrixArray[$index] = array_map("intval", explode(" ", $value));
        }
    }
}
