<?php

class Robot
{
    public function __construct() {
        if (!isset($GLOBALS['robots'])) {
            global $robots;
            $robots = [];
        }

        $this->name = null;
        $this->verifyName();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function verifyName(): string
    {
        $str_letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $str_digit = '0123456789';

        do {
            $this->name = substr(str_shuffle($str_letter), 0,2).''.substr(str_shuffle($str_digit), 0, 3);
        } while (in_array($this->name, $GLOBALS['robots']));

        array_push($GLOBALS['robots'], $this->name);
        return $this->name;
    }

    public function reset()
    {
        $this->verifyName();
    }
}
