<?php

require_once 'helloWorld.php';
require_once 'reverseString.php';
require_once 'Robot.php';
require_once 'maskify.php';
require_once 'Clock.php';
require_once 'Grade-School.php';
require_once 'Matrix.php';
require_once 'DungeonsDragonsCharacter.php';

try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $port= '3307';
    $dsn = "mysql:host=$host;port=$port;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);

    $dndChar = new DndCharacter();
    var_dump($dndChar);
    echo '<br />';
} catch (\Throwable $t) {
    var_dump($t);
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}
